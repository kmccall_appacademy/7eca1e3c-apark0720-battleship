class HumanPlayer
  attr_reader :name

  def initialize(name = "Alex")
    @name = name
  end

  def get_play
    puts "Enter a position to attack! (ex. '0(row), 1(col))"
    gets.chomp.split(",").map(&:to_i)
  end
end

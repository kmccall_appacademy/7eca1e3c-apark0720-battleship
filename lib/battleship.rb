require_relative 'board'
require_relative 'player'

class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
  end

  def attack(position)
    @board[position] = :x
  end

  def count
    @board.count
  end

  def display_status #better scale design
    range_scale = (0...@board.grid[0].length).to_a
    puts range_scale.join(" ")
    @board.grid.each do |row|
      puts row.map { |ele| ele == nil ? "-" : ele }.join(" ")
    end
  end

  def game_over?
    @board.won?
  end

  def play_turn
    move = @player.get_play
    self.attack(move)
  end

  def play
    puts "Let's play <Battleship>!!!"
    @board.place_random_ship until @board.count == 2
    until game_over?
      display_status
      play_turn
    end
    conclude
  end

  def conclude
    puts "Game Over!"
    if @board.count == 0
      puts "Congratulations #{@player.name}! You attacked all the ships!"
    else
      puts "You lost:( There are #{@board.count} ships remaining."
      @board.grid.each do |row|
        puts row.map { |ele| ele == nil ? "-" : ele }.join(" ")
      end
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  board = Board.new
  puts "What's your name?"
  name = gets.chomp
  player = HumanPlayer.new(name)
  game = BattleshipGame.new(player, board)
  game.play
end
